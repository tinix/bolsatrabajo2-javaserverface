
package prueba.ciclodevida;

import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
        

/**
 *
 * @author tinix
 */


public abstract class DepuracionListerner implements javax.faces.event.PhaseListener {
    private static final Log log = LogFactory.getLog(DepuracionListerner.class);
    
    public void afterPhate(PhaseEvent phasesEvent) {
        if(log.isInfoEnabled()) {
            log.info("AFTER PHASE: " + phasesEvent.getPhaseId().toString());
        }
    }
    
    @Override
    public void beforePhase(PhaseEvent phaseEvent) {
        if (log.isInfoEnabled()) {
            log.info("BEFORE PHASE: " + phaseEvent.getPhaseId().toString());
        }
    }
    
    @Override
    public PhaseId getPhaseId(){
        return PhaseId.ANY_PHASE;
    }
}
